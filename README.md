# OpenML dataset: LT_Finance_Holdings_Ltd_Stock_Price_2017_to_2020

https://www.openml.org/d/43558

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset is a playground for fundamental and technical analysis. This can serve as basic tutorial for time-series data analysis.
Content
Dataset consists of following files:
LTFinanceHoldingsLtdStockPrice2017to2020.csv: The data related to LT Finance Holdings Ltd Stock Price from Feb 2017 to Feb 2020 and contains 13 columns.
Acknowledgements
Prices were fetched from "bseindia" databases.
Inspiration
Here is couple of things one could try out with this data:

One day ahead prediction: Rolling Linear Regression, ARIMA, SARIMA, LSTM
Momentum/Mean-Reversion Strategies

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43558) of an [OpenML dataset](https://www.openml.org/d/43558). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43558/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43558/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43558/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

